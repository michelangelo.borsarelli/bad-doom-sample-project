﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinearMovement : Movement
{
    public Vector3 Direction;
    public Space RelativeTo;

    public float Speed;

    protected virtual void OnValidate()
    {
        Direction.Normalize();
    }

    public override void MoveUpdate(Transform tr)
    {
        tr.Translate(Direction * Speed * Time.deltaTime, RelativeTo);
    }
}
