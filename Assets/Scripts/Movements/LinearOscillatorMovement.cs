﻿using UnityEngine;

public class LinearOscillatorMovement : LinearMovement
{
    #region Serialized variables
    
    public Vector3 Axis;
    public Space OscillatorRelativeTo;

    [Space]

    public float Amplitude;
    public float Frequency;
    public float Phase;


    #endregion

    #region Private variables

    private float m_ActualPhase;

    #endregion


    #region MonoBehaviour cycle

    protected override void OnValidate()
    {
        base.OnValidate();
        Axis.Normalize();
    }

    #endregion


    #region Protected and private methods

    /// <summary>
    /// Makes the object oscillate on the specified axis with the specified oscillation parameters.
    /// </summary>
    public override void MoveUpdate(Transform tr)
    {
        base.MoveUpdate(tr);

        tr.Translate(Amplitude * (Mathf.Sin(Mathf.Rad2Deg * (Frequency * Time.time + m_ActualPhase)) - Mathf.Sin(Mathf.Rad2Deg * (Frequency * (Time.time - Time.deltaTime) + m_ActualPhase))) * Axis, OscillatorRelativeTo);
    }

    /// <summary>
    /// Calculates the actual initial phase, considering current time.
    /// </summary>
    public override void Init(Transform tr)
    {
        m_ActualPhase = Phase - Frequency * Time.time;
    }

    #endregion
}
