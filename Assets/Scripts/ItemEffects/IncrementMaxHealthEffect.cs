﻿using UnityEngine;

public class IncrementMaxHealthEffect : ItemEffect
{
    [SerializeField] private float m_MaxHealthIncrement;

    protected override string Description => "Permanently increment Max Health by " + m_MaxHealthIncrement;

    public override void Apply(PlayerController user)
    {
        user.MaxHealth += m_MaxHealthIncrement;
    }
}
