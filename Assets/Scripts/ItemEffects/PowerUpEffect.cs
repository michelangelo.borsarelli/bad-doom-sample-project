﻿using UnityEngine;

public class PowerUpEffect : ItemEffect
{
    [SerializeField] private float m_PowerUpTime;

    protected override string Description => "Power up for " + m_PowerUpTime + " seconds";

    public override void Apply(PlayerController user)
    {
        user.PowerUp(m_PowerUpTime);
    }
}
