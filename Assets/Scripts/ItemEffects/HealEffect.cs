﻿using UnityEngine;

public class HealEffect : ItemEffect
{
    [SerializeField] private float m_HealthAmount;

    protected override string Description => "Heal by " + m_HealthAmount + " HP";

    public override void Apply(PlayerController user)
    {
        user.Health += m_HealthAmount;
    }
}
