﻿using System.Collections.Generic;

namespace CorgiLabs.Utility
{
    #region Delegates

    public delegate bool Validation<T>(T arg);
    public delegate T Generator<T>();
    public delegate void Initializator<T>(T arg);

    #endregion

    public class ObjectPool<T> where T : class
    {
        #region Private variables

        /// <summary>
        /// Actual objects pool, containing all available and unavailable instances.
        /// </summary>
        private List<T> m_Pool;

        /// <summary>
        /// Validator delegate used to determine whether an instance is available or not.
        /// </summary>
        private Validation<T> m_Validator;

        /// <summary>
        /// Generator delegate used to generate a new object instance when needed.
        /// </summary>
        private Generator<T> m_ObjectGenerator;

        /// <summary>
        /// Initializes an object in the pool just before returning it, when required.
        /// </summary>
        private Initializator<T> m_ObjectInitializator;

        /// <summary>
        /// Should the pool be able to expand its size?
        /// </summary>
        private bool m_CanExpand;

        /// <summary>
        /// Maximum size of the pool.
        /// </summary>
        private int m_MaxSize;

        #endregion

        #region Constructor

        /// <summary>
        /// Generates a new ObjectPool instance.
        /// </summary>
        /// <param name="inValidator">Validator delegate, used to determine whether an instance in the pool is available or not. Returns true if available.</param>
        /// <param name="inGenerator">Generator delegate, used to generate new object instances to add to the pool.</param>
        /// <param name="inInitializator">Initialization delegate, used to initialize an object before returning it.</param>
        /// <param name="inCanExpand">Should the pool be able to expand its size?</param>
        /// <param name="inInitialAmount">Initial amount of instances in the pool.</param>
        /// <param name="inMaxSize">Maximum pool size. If zero, the pool is unrestricted.</param>
        public ObjectPool(Validation<T> inValidator, Generator<T> inGenerator, Initializator<T> inInitializator = null, bool inCanExpand = true, int inInitialAmount = 0, int inMaxSize = 0)
        {
            // Init data
            m_Validator = inValidator;
            m_ObjectGenerator = inGenerator;
            m_ObjectInitializator = inInitializator;
            m_MaxSize = inMaxSize;
            m_CanExpand = inCanExpand;

            // Init pool
            m_Pool = new List<T>();

            // Fill it with initial instances if needed
            if (inInitialAmount > 0)
            {
                for (int i = 0; i < inInitialAmount; i++)
                    m_Pool.Add(m_ObjectGenerator.Invoke());
            }
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Gets an object from the pool.
        /// </summary>
        /// <returns></returns>
        public T Get()
        {
            // For each object in the pool, check if it's available; if it is, return it
            foreach (T obj in m_Pool)
            {
                if (m_Validator.Invoke(obj))
                {
                    m_ObjectInitializator?.Invoke(obj);
                    return obj;
                }
            }

            // If no object was found, add new one, but only if the pool can expand and either it's unrestricted or its size is less than the maximum size
            if (m_CanExpand && (m_MaxSize == 0 || m_Pool.Count < m_MaxSize))
            {
                // Generate new instance
                T newObj = m_ObjectGenerator.Invoke();

                // Add it to the pool
                m_Pool.Add(newObj);

                m_ObjectInitializator?.Invoke(newObj);

                // return it
                return newObj;
            }
            else
            {
                // No instance was found or could be instantiated: return null
                return null;
            }
        }

        /// <summary>
        /// Does the Pool contain the specified object?
        /// </summary>
        /// <param name="obj">Object to check for.</param>
        /// <returns>True if the Pool contains the object.</returns>
        public bool Contains(T obj)
        {
            return m_Pool.Contains(obj);
        }

        #endregion
    }
}