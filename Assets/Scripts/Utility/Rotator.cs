﻿using UnityEngine;

public class Rotator : MonoBehaviour
{
    public new Transform transform { get; private set; }

    [SerializeField] private float m_RotationSpeed;

    void Awake()
    {
        transform = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Vector3.up, m_RotationSpeed * Time.deltaTime);
    }
}
