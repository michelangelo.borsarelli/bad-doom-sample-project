﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.AI;

[System.Serializable]
public class FloatEvent : UnityEvent<float> { }

public class Entity : MonoBehaviour, IDamageable
{
    public FloatEvent OnDamaged;

    [SerializeField] private float m_Damage;

    [SerializeField] private float m_MaxHealth;
    [SerializeField] private float m_ChaseRadius;
    [SerializeField, ReadOnly] private float m_Health;

    public Transform Target;

    public NavMeshAgent Agent { get; private set; }

    public AnimatorHandler AnimatorHandler { get; private set; }

    private float m_SqrDistanceToTarget;

    private void Awake()
    {
        Agent = GetComponent<NavMeshAgent>();
        AnimatorHandler = GetComponentInChildren<AnimatorHandler>();
    }

    private void Start()
    {
        m_Health = m_MaxHealth;
    }

    private void Update()
    {
        m_SqrDistanceToTarget = (transform.position - Target.position).sqrMagnitude;

        if (m_SqrDistanceToTarget > Agent.stoppingDistance * Agent.stoppingDistance && m_SqrDistanceToTarget < m_ChaseRadius * m_ChaseRadius)
            Agent.SetDestination(Target.position);
    }

    public void TakeDamage(float damage)
    {
        m_Health = Mathf.Clamp(m_Health - damage, 0, m_MaxHealth);
        OnDamaged.Invoke(damage);

        if (m_Health == 0)
        {
            Debug.Log("Dead!");
            gameObject.SetActive(false);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        other.GetComponentInParent<PlayerController>()?.TakeDamage(m_Damage);
    }
}
