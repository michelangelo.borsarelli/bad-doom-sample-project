﻿using UnityEngine;

public enum DamageType
{
    Standard,
    Fire,
    Ice,
    Shock
}

[System.Serializable]
public struct DamageOutput
{
    public float Damage;
    public DamageType Type;
}

[CreateAssetMenu(menuName = "Weapon")]
public class Weapon : ScriptableObject
{
    public WeaponClass Class;
    [Space]
    public WeaponModel Model;
    public SoundEffect ShootSound;
    [Space]
    public WeaponShootMethod ShootingMethod;
    public float FireDelay;
    [Space]
    public DamageOutput OutDamage;


    private float m_NextShotTime;

    private WeaponModel m_CachedModel;
    private PlayerController m_Owner;

    private void OnEnable()
    {
        m_NextShotTime = 0;
    }

    public bool Shoot()
    {
        if (Time.time < m_NextShotTime)
            return false;

        m_NextShotTime = Time.time + FireDelay;
        ShootingMethod.Shoot(OutDamage.Damage, m_CachedModel, m_Owner);
        m_CachedModel.Animator.SetTrigger("Shoot");
        ShootSound.Play(m_CachedModel.SpawnPoints[0].position);
        return true;
    }

    public void Equip(PlayerController pc)
    {
        if (m_CachedModel == null)
            m_CachedModel = Instantiate(Model);

        m_Owner = pc;
        m_CachedModel.transform.SetParent(pc.WeaponHandle, false);
        m_CachedModel.transform.SetPositionAndRotation(pc.WeaponHandle.position, pc.WeaponHandle.rotation);
        m_CachedModel.gameObject.SetActive(true);
    }

    public void Unequip()
    {
        m_Owner = null;
        m_CachedModel.gameObject.SetActive(false);
        m_CachedModel.transform.SetParent(null);
    }
}
