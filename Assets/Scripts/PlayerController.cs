﻿using UnityEngine;
using System.Threading.Tasks;
using UnityStandardAssets.Characters.FirstPerson;

public class PlayerController : MonoBehaviour, IDamageable
{
    public event System.Action OnHealthChanged;
    public event System.Action OnMaxHealthChanged;
    public event System.Action OnItemChanged;
    public event System.Action OnPoweredUpChanged;
    public event System.Action OnDamaged;
    public event System.Action OnWeaponChanged;

    public Weapon CurrentWeapon { get; private set; }

    public new Transform transform { get; private set; }
    public new Rigidbody rigidbody { get; private set; }

    private Item m_CurrentItem;
    public Item CurrentItem
    {
        get => m_CurrentItem;
        set
        {
            m_CurrentItem = value;
            OnItemChanged?.Invoke();
        }
    }

    public Transform WeaponHandle;

    public Weapon StartingWeapon;

    public Camera FpsCamera;

    [SerializeField]
    private float m_MaxHealth;
    public float MaxHealth
    {
        get => m_MaxHealth;
        set
        {
            m_MaxHealth = value;
            m_Health = Mathf.Clamp(m_Health, 0, m_MaxHealth);
            OnMaxHealthChanged?.Invoke();
        }
    }
    private RigidbodyFirstPersonController m_FpsController;

    [SerializeField]
    private float m_PoweredUpSpeedMultiplier;

    [SerializeField]
    private AudioSource m_SfxSource;


    private float m_OriginalForwardSpeed;
    private float m_OriginalBackwardSpeed;
    private float m_OriginalStrafeSpeed;


    [SerializeField, ReadOnly]
    private bool m_IsPoweredUp;
    public bool IsPoweredUp
    {
        get => m_IsPoweredUp;
        set
        {
            m_IsPoweredUp = value;
            m_FpsController.movementSettings.ForwardSpeed = m_OriginalForwardSpeed;
            m_FpsController.movementSettings.BackwardSpeed = m_OriginalBackwardSpeed;
            m_FpsController.movementSettings.StrafeSpeed = m_OriginalStrafeSpeed;

            if (value)
            {
                m_FpsController.movementSettings.ForwardSpeed *= m_PoweredUpSpeedMultiplier;
                m_FpsController.movementSettings.BackwardSpeed *= m_PoweredUpSpeedMultiplier;
                m_FpsController.movementSettings.StrafeSpeed *= m_PoweredUpSpeedMultiplier;
            }

            OnPoweredUpChanged?.Invoke();
        }
    }

    [SerializeField]
    private float m_InvincibilityFrame;

    [SerializeField, ReadOnly]
    private float m_Health;
    public float Health
    {
        get => m_Health;
        set
        {
            m_Health = Mathf.Clamp(value, 0, MaxHealth);
            OnHealthChanged?.Invoke();
        }
    }

    private float m_NextDamageTime = 0.0f;

    private void Awake()
    {
        transform = GetComponent<Transform>();
        rigidbody = GetComponent<Rigidbody>();

        m_FpsController = GetComponent<RigidbodyFirstPersonController>();
        FpsCamera = GetComponentInChildren<Camera>();
        EquipWeapon(StartingWeapon);
    }

    private void Start()
    {
        m_Health = m_MaxHealth;

        m_OriginalBackwardSpeed = m_FpsController.movementSettings.BackwardSpeed;
        m_OriginalForwardSpeed = m_FpsController.movementSettings.ForwardSpeed;
        m_OriginalStrafeSpeed = m_FpsController.movementSettings.StrafeSpeed;
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.Mouse0) && CurrentWeapon != null)
            CurrentWeapon.Shoot();

        if (Input.GetKeyDown(KeyCode.E) && CurrentItem != null)
            UseItem();
    }

    private void UseItem()
    {
        CurrentItem.Use(this);
        CurrentItem = null;
    }

    public void EquipWeapon(Weapon weap)
    {
        if (weap == CurrentWeapon)
        {
            weap.Class.PickupSfx.Play(m_SfxSource);
            return;
        }

        CurrentWeapon?.Unequip();
        weap.Equip(this);
        CurrentWeapon = weap;
        weap.Class.PickupSfx.Play(m_SfxSource);
        OnWeaponChanged?.Invoke();
    }

    public void TakeDamage(float damage)
    {
        if (m_IsPoweredUp || Time.time < m_NextDamageTime)
            return;

        m_NextDamageTime = Time.time + m_InvincibilityFrame;
        Health -= damage;
        OnDamaged?.Invoke();
    }

    public async void PowerUp(float time)
    {
        IsPoweredUp = true;

        await Task.Delay(System.TimeSpan.FromSeconds(time));

        IsPoweredUp = false;
    }
}
