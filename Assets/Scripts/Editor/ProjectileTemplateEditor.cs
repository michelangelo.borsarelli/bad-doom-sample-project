﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ProjectileTemplate))]
public class ProjectileTemplateEditor : Editor
{
    SerializedProperty m_Model;
    SerializedProperty m_MovementType;
    SerializedProperty m_Movement;

    Editor m_MovementEditor;

    private void OnEnable()
    {
        m_Model = serializedObject.FindProperty("Model");
        m_MovementType = serializedObject.FindProperty("MovementType");
        m_Movement = serializedObject.FindProperty("Movement");

        if (m_Movement.objectReferenceValue != null)
            m_MovementEditor = CreateEditor(m_Movement.objectReferenceValue);
    }
    
    public override void OnInspectorGUI()
    {
        EditorGUI.BeginChangeCheck();

        EditorGUILayout.PropertyField(m_Model);

        EditorGUI.BeginChangeCheck();
        EditorGUILayout.PropertyField(m_MovementType);
        if(EditorGUI.EndChangeCheck())
        {
            if (m_Movement.objectReferenceValue != null)
            {
                DestroyImmediate(m_Movement.objectReferenceValue, true);
                AssetDatabase.SaveAssets();
            }


            System.Type t = System.Type.GetType(m_MovementType.FindPropertyRelative("_classRef").stringValue);

            if (t != null)
            {
                Movement _mov = CreateInstance(t) as Movement;
                _mov.name = t.Name;

                AssetDatabase.AddObjectToAsset(_mov, serializedObject.targetObject);
                AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(_mov));

                m_Movement.objectReferenceValue = _mov;
                m_MovementEditor = CreateEditor(_mov);
            }
            else
                m_MovementEditor = null;

            serializedObject.ApplyModifiedProperties();
        }

        if (EditorGUI.EndChangeCheck())
            serializedObject.ApplyModifiedProperties();

        if (m_MovementEditor)
        {
            EditorGUILayout.BeginVertical(GUI.skin.box);
            m_MovementEditor.OnInspectorGUI();
            EditorGUILayout.EndVertical();
        }
    }
}
