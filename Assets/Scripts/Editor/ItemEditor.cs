﻿using UnityEditor;
using CorgiLabs.EditorUtils;

[CustomEditor(typeof(Item))]
public class ItemEditor : Editor
{
    SerializedProperty m_DisplayName;
    SerializedProperty m_Icon;
    InternalScriptableObjectList<ItemEffect> m_Effects;

    private void OnEnable()
    {
        m_DisplayName = serializedObject.FindProperty("DisplayName");
        m_Icon = serializedObject.FindProperty("Icon");
        m_Effects = new InternalScriptableObjectList<ItemEffect>(serializedObject.FindProperty("Effects"), null, null, false)
        {
            ElementLabelContentGetter = eff => eff.DisplayDescription,
            TypeNameSuffix = "Effect",
            ElementHeight = 2.0f * EditorGUIUtility.singleLineHeight,
        };
    }

    public override void OnInspectorGUI()
    {
        EditorGUI.BeginChangeCheck();

        EditorGUILayout.PropertyField(m_DisplayName);
        EditorGUILayout.PropertyField(m_Icon);

        EditorGUILayout.Space();

        if (EditorGUI.EndChangeCheck())
            serializedObject.ApplyModifiedProperties();

        m_Effects.DoLayoutList();
    }
}
