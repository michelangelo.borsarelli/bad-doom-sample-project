﻿using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(DamageOutput))]
public class DamageOutputDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginChangeCheck();

        EditorGUI.DrawRect(position, Color.white);

        position.y += 0.2f * EditorGUIUtility.singleLineHeight;
        position.height = EditorGUIUtility.singleLineHeight;
        position.width = (position.width - 50) / 3.0f;

        EditorGUI.LabelField(position, property.displayName, EditorStyles.boldLabel);

        position.x += position.width;

        SerializedProperty damageProperty = property.FindPropertyRelative("Damage");
        SerializedProperty typeProperty = property.FindPropertyRelative("Type");

        damageProperty.floatValue = EditorGUI.FloatField(position, damageProperty.floatValue);

        position.x += position.width;

        DamageType t = (DamageType)EditorGUI.EnumPopup(position, (DamageType)typeProperty.enumValueIndex);

        typeProperty.enumValueIndex = (int)t;

        position.x += position.width;
        position.y -= 0.2f * EditorGUIUtility.singleLineHeight;
        position.height = GetPropertyHeight(property, label);
        position.width = 50;

        Color c = Color.black;

        switch(t)
        {
            case DamageType.Standard:
                c = Color.gray;
                break;

            case DamageType.Fire:
                c = Color.red;
                break;

            case DamageType.Ice:
                c = Color.cyan;
                break;

            case DamageType.Shock:
                c = Color.yellow;
                break;

            default:
                break;

        }

        Color _guiColor = GUI.backgroundColor;
        GUI.backgroundColor = c;
        GUI.Box(position, t.ToString());
        GUI.backgroundColor = _guiColor;

        if (EditorGUI.EndChangeCheck())
            property.serializedObject.ApplyModifiedProperties();
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return 1.4f * EditorGUIUtility.singleLineHeight;
    }
}
