﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(ReadOnlyAttribute))]
public class ReadOnlyPropertyDrawer : PropertyDrawer
{
    bool _aux;

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        _aux = GUI.enabled;
        GUI.enabled = false;
        EditorGUI.PropertyField(position, property);
        GUI.enabled = _aux;
    }
}
