﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Pad))]
public class PadEditor : Editor
{
    SerializedProperty m_Icon;
    SerializedProperty m_BaseRenderer;
    SerializedProperty m_Type;

    private Editor m_EffectEditor;

    private void OnEnable()
    {
        m_Icon = serializedObject.FindProperty("Icon");
        m_BaseRenderer = serializedObject.FindProperty("BaseRenderer");
        m_Type = serializedObject.FindProperty("Type");

        PadEffect effect = (target as Pad).GetComponent<PadEffect>();

        if (effect)
            m_EffectEditor = CreateEditor(effect);
    }

    public override void OnInspectorGUI()
    {
        EditorGUI.BeginChangeCheck();

        EditorGUILayout.PropertyField(m_Icon);
        EditorGUILayout.PropertyField(m_BaseRenderer);

        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);

        EditorGUI.BeginChangeCheck();
        EditorGUILayout.PropertyField(m_Type);
        if (EditorGUI.EndChangeCheck())
        {
            PadEffect effect = (target as Pad).GetComponent<PadEffect>();

            if (effect)
                DestroyImmediate(effect);

            if (m_Type.objectReferenceValue != null)
            {
                Component c = (target as Pad).gameObject.AddComponent((m_Type.objectReferenceValue as PadType).EffectType);
                c.hideFlags = HideFlags.HideInInspector;
                m_EffectEditor = CreateEditor(c);

                (m_Icon.objectReferenceValue as SpriteRenderer).sprite = (m_Type.objectReferenceValue as PadType).Icon;
                (target as Pad).GetComponentInChildren<Light>().color = (m_Type.objectReferenceValue as PadType).BaseColor;
            }
            else
                m_EffectEditor = null;
        }

        if (EditorGUI.EndChangeCheck())
                serializedObject.ApplyModifiedProperties();

        if (m_EffectEditor)
        {
            EditorGUILayout.BeginVertical(GUI.skin.box);
            m_EffectEditor.OnInspectorGUI();
            EditorGUILayout.EndVertical();
        }
    }
}
