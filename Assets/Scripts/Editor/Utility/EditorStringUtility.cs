﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CorgiLabs.EditorUtils
{
    public static class EditorStringUtility
    {
        /// <summary>
        /// Splits the input string at capital letters, separating words with whitespaces.
        /// </summary>
        /// <param name="input">String to split.</param>
        /// <returns>Split string.</returns>
        public static string CapitalSplit(this string input)
        {
            string result = string.Empty;

            foreach (var ch in input)
            {
                if (char.IsUpper(ch) && result.Length > 0)
                    result += ' ';

                result += ch;
            }

            return result;
        }

        /// <summary>
        /// Trims a prefix from a string.
        /// </summary>
        /// <param name="input">String to trim.</param>
        /// <param name="prefix">Prefix to be trimmed.</param>
        /// <returns></returns>
        public static string TrimPrefix(this string input, string prefix)
        {
            if (input.Substring(0, prefix.Length) == prefix)
                return input.Substring(prefix.Length);
            else
                return input;
        }

        /// <summary>
        /// Trims a suffix from a string.
        /// </summary>
        /// <param name="input">String to trim.</param>
        /// <param name="prefix">Suffix to be trimmed.</param>
        /// <returns></returns>
        public static string TrimSuffix(this string input, string suffix)
        {
            int pos = input.Length - suffix.Length;

            if (pos > -1 && input.Substring(pos) == suffix)
                return input.Substring(0, pos);
            else
                return input;
        }

        /// <summary>
        /// Is a string null or empty?
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static bool IsNullOrEmpty(this string s)
        {
            return s == null || s == string.Empty;
        }
    }
}
