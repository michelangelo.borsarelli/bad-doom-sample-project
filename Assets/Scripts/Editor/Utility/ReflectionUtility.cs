﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Reflection;

namespace CorgiLabs.Utility
{
    public class ReflectionUtility
    {
        /// <summary>
        /// Finds all types that represent concrete subclasses of a type in its same assembly.
        /// </summary>
        /// <typeparam name="T">Superclass type.</typeparam>
        /// <returns>A collection of all types found.</returns>
        public static List<Type> FindConcreteSubtypes<T>() where T : class
        {
            // Init collection
            List<Type> returnTypes = new List<Type>();

            // Get assembly types
            Type[] types = Assembly.GetAssembly(typeof(T)).GetTypes();

            // Add sublcass types to the collection
            foreach (Type t in types)
            {
                if (t.IsClass && !t.IsAbstract && t.IsSubclassOf(typeof(T)))
                    returnTypes.Add(t);
            }

            // Return filled collection
            return returnTypes;
        }
    }
}
