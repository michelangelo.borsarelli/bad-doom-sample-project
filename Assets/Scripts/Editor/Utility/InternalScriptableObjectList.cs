﻿using UnityEngine;
using UnityEditorInternal;
using UnityEditor;
using System;
using CorgiLabs.Utility;

namespace CorgiLabs.EditorUtils
{
    public class InternalScriptableObjectList<T> where T : ScriptableObject
    {
        #region Delegates definition

        public delegate UnityEditor.Editor EditorCreationDelegate(UnityEngine.Object obj);
        public delegate string LabelContentDelegate(T obj);

        #endregion

        #region Events

        /// <summary>
        /// Event invoked on a List content change.
        /// </summary>
        public event System.Action OnChanged;

        #endregion

        #region Public properties

        public float Height { get { return m_Native.GetHeight() + 35.0f; } }

        public bool HideSubObjects { get; set; }

        #endregion

        #region Public variables

        /// <summary>
        /// Override delegate used to retrieve the element Label content.
        /// If no delegate is provided, the Label content will match the Object's name.
        /// </summary>
        public LabelContentDelegate ElementLabelContentGetter;

        /// <summary>
        /// Delegate used to generate the Editor instance for the sub-Object Inspector draw callback.
        /// If no delegate is provided, the Editor will be just be the one associated with the sub-Object's Type.
        /// </summary>
        public EditorCreationDelegate EditorGenerator;

        /// <summary>
        /// Content of the header label.
        /// </summary>
        public string HeaderContent;

        /// <summary>
        /// Height of a List element.
        /// </summary>
        public float ElementHeight = 1.0f;

        /// <summary>
        /// Optional trimmed prefix of Type names.
        /// </summary>
        public string TypeNamePrefix = "";

        /// <summary>
        /// Optional trimmed prefix of Type names.
        /// </summary>
        public string TypeNameSuffix = "";

        /// <summary>
        /// Anchor setting for element Labels.
        /// </summary>
        public TextAnchor LabelAnchor;

        #endregion

        #region Private variables

        /// <summary>
        /// Native ReorderableList instance.
        /// </summary>
        private ReorderableList m_Native;

        /// <summary>
        /// Available Types for sub-Objects.
        /// This list is filtered at construction time, making sure it contains only concrete T subtypes.
        /// </summary>
        private Type[] m_AvailableTypes;

        /// <summary>
        /// Generated Editor for currently selected sub-Object, used for sub-Inspector drawing.
        /// </summary>
        private Editor m_SelectedEditor;

        /// <summary>
        /// Auxiliary GUIStyle variable.
        /// </summary>
        private GUIStyle m_LabelStyle;

        #endregion

        #region Constructor

        public InternalScriptableObjectList(SerializedProperty inSerializedProperty,
                                            Type[] inAvailableTypes = null,
                                            ReorderableList.ElementCallbackDelegate drawElementOverride = null,
                                            bool hideSubObjects = true)
        {
            // if a list of Types is provided, filter it by considering only CONCRETE subclasses of T;
            // otherwise just get all concrete subtypes of T
            m_AvailableTypes = inAvailableTypes != null ? Array.FindAll(inAvailableTypes, t => t.IsSubclassOf(typeof(T)) && !t.IsAbstract) : ReflectionUtility.FindConcreteSubtypes<T>().ToArray();

            HideSubObjects = hideSubObjects;

            // Set default delegates, to be eventually overriden later on
            EditorGenerator = obj => Editor.CreateEditor(obj);
            ElementLabelContentGetter = obj => obj.name;

            // Setup native ReorderableList
            m_Native = new ReorderableList(inSerializedProperty.serializedObject, inSerializedProperty, true, true, true, true)
            {
                drawElementCallback = drawElementOverride ?? DrawEntry,
                onAddDropdownCallback = AddObjectDropdown,
                elementHeightCallback = GetElementHeight,
                onRemoveCallback = RemoveSelectedObject,
                drawHeaderCallback = DrawListHeader,
                onSelectCallback = OnSelect,
            };
        }

        #endregion

        #region Public interface

        /// <summary>
        /// Draws the Reorderable List.
        /// </summary>
        public void DoLayoutList()
        {
            // Delegate to Native draw method
            m_Native.DoLayoutList();

            EditorGUILayout.Space();

            // Draw selected sub-Object's Inspector
            if (m_SelectedEditor)
                m_SelectedEditor.OnInspectorGUI();

            EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
        }

        public void DoList(Rect rect)
        {
            Rect listRect = rect;
            listRect.height = m_Native.GetHeight();

            m_Native.DoList(rect);

            rect.height = 35.0f;
            rect.y += listRect.height;

            if (GUI.Button(rect, "Edit"))
                Selection.activeObject = m_Native.serializedProperty.GetArrayElementAtIndex(m_Native.index).objectReferenceValue;
        }

        #endregion


        #region Native callbacks

        /// <summary>
        /// Callback for drawing a List entry.
        /// </summary>
        /// <param name="rect"></param>
        /// <param name="index"></param>
        /// <param name="isActive"></param>
        /// <param name="isFocused"></param>
        private void DrawEntry(Rect rect, int index, bool isActive, bool isFocused)
        {
            // Define Label Rect
            // > 90% height
            // > 10% padding

            Rect tempRect = rect;
            tempRect.height = 0.9f * rect.height;

            tempRect.y = rect.y + 0.05f * rect.height;

            // Set style
            m_LabelStyle = GUI.skin.label;
            m_LabelStyle.alignment = LabelAnchor;

            // Draw element Label
            EditorGUI.LabelField(tempRect, ElementLabelContentGetter.Invoke(m_Native.serializedProperty.GetArrayElementAtIndex(index).objectReferenceValue as T), m_LabelStyle);
        }

        /// <summary>
        /// Removes the currently selected List entry.
        /// </summary>
        /// <param name="list">ReorderableList on which the entry is selected.</param>
        private void RemoveSelectedObject(ReorderableList list)
        {
            // Cache property for destruction
            SerializedProperty propertyToRemove = m_Native.serializedProperty.GetArrayElementAtIndex(list.index);

            // Destroy asset
            UnityEngine.Object.DestroyImmediate(propertyToRemove.objectReferenceValue, true);
            AssetDatabase.SaveAssets();


            // Remove from list
            propertyToRemove.objectReferenceValue = null;
            m_Native.serializedProperty.DeleteArrayElementAtIndex(list.index);

            // Clear selected Editor, as the selected entry has been removed
            m_SelectedEditor = null;

            // Apply modified properties, otherwise changes won't be saved
            list.serializedProperty.serializedObject.ApplyModifiedProperties();

            // Invoke OnChanged event
            OnChanged?.Invoke();
        }

        /// <summary>
        /// Draws a dropdown menu from which to choose a sub-Object to add among available Types.
        /// </summary>
        /// <param name="buttonRect"></param>
        /// <param name="list"></param>
        private void AddObjectDropdown(Rect buttonRect, ReorderableList list)
        {
            // Generate the dropdown menu
            GenericMenu menu = new GenericMenu();

            // Add Menu entries
            foreach (Type t in m_AvailableTypes)
                menu.AddItem(new GUIContent(FormatTypeName(t.Name)), false, AddSelectedType, t);

            // Show menu
            menu.ShowAsContext();
        }

        /// <summary>
        /// Callback for the Add dropdown menu; instantiate the selected sub-Object Type, saves it to an asset and adds it to the List.
        /// </summary>
        /// <param name="objType"></param>
        private void AddSelectedType(object objType)
        {
            // Recast to Type
            Type t = objType as Type;

            SerializedProperty property = m_Native.serializedProperty;

            // Instantiate the sub-Object and initialize it
            UnityEngine.Object newObj = ScriptableObject.CreateInstance(t);
            newObj.name = FormatTypeName(t.Name);
            newObj.hideFlags = HideSubObjects ? HideFlags.HideInHierarchy : HideFlags.None; // Hide it in Project View

            // Save the new Action to an asset under the parent Object's asset
            AssetDatabase.AddObjectToAsset(newObj, property.serializedObject.targetObject);
            AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(newObj));

            // Add it to the List
            property.arraySize++;
            property.GetArrayElementAtIndex(property.arraySize - 1).objectReferenceValue = newObj;

            // Apply modified properties, otherwise changes won't be serialized
            property.serializedObject.ApplyModifiedProperties();

            // Invoke OnChanged event
            OnChanged?.Invoke();
        }

        /// <summary>
        /// Callback for Reorderable Action List header drawing.
        /// </summary>
        /// <param name="rect"></param>
        private void DrawListHeader(Rect rect)
        {
            EditorGUI.LabelField(rect, m_Native.serializedProperty.displayName, EditorStyles.boldLabel);
        }

        /// <summary>
        /// Gets the height of a List element.
        /// </summary>
        /// <param name="index">Index of the element.</param>
        /// <returns></returns>
        private float GetElementHeight(int index)
        {
            return ElementHeight;
        }

        /// <summary>
        /// Callback for List entry selection.
        /// </summary>
        /// <param name="list"></param>
        private void OnSelect(ReorderableList list)
        {
            // Generate Editor for sub-Object Inspector draw
            m_SelectedEditor = EditorGenerator.Invoke(list.serializedProperty.GetArrayElementAtIndex(list.index).objectReferenceValue);
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Formats a sub-Object Type name, by trimming the set prefix and suffix.
        /// </summary>
        /// <param name="typeName">Original name of the Type.</param>
        /// <returns>Formatted Type name.</returns>
        private string FormatTypeName(string typeName)
        {
            return typeName.TrimPrefix(TypeNamePrefix).TrimSuffix(TypeNameSuffix).CapitalSplit();
        }

        #endregion
    }
}
