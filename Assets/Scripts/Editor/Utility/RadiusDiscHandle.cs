﻿using UnityEngine;
using UnityEditor;

namespace CorgiLabs.EditorUtils
{
    public class RadiusDiscHandle
    {
        private const float DEFAULT_ALPHA = 0.3f;

        private static readonly Color DEFAULT_COLOR = Color.white;


        private Vector3 m_StartingWorldMousePos;

        private Vector2 m_CurrentMousePos;

        private Vector3 m_DiffDir;

        private float m_StartingRadius;

        private Color m_Color;
        private Color m_SelectedColor;
        private Color m_PreselectionColor;

        public RadiusDiscHandle(float alpha = DEFAULT_ALPHA)
        {
            m_Color = DEFAULT_COLOR;
            m_SelectedColor = Handles.selectedColor;
            m_PreselectionColor = Handles.preselectionColor;

            m_Color.a = alpha;
            m_SelectedColor.a = alpha;
            m_PreselectionColor.a = alpha;
        }

        public RadiusDiscHandle(Color inColor, float alpha = DEFAULT_ALPHA)
        {
            m_Color = inColor;
            m_SelectedColor = Handles.selectedColor;
            m_PreselectionColor = Handles.preselectionColor;

            m_Color.a = alpha;
            m_SelectedColor.a = alpha;
            m_PreselectionColor.a = alpha;
        }

        public RadiusDiscHandle(Color inColor, Color inSelectedColor, float alpha = DEFAULT_ALPHA)
        {
            m_Color = inColor;
            m_SelectedColor = inSelectedColor;
            m_PreselectionColor = Handles.preselectionColor;

            m_Color.a = alpha;
            m_SelectedColor.a = alpha;
            m_PreselectionColor.a = alpha;
        }

        public RadiusDiscHandle(Color inColor, Color inSelectedColor, Color inPreselectionColor, float alpha = DEFAULT_ALPHA)
        {
            m_Color = inColor;
            m_SelectedColor = inSelectedColor;
            m_PreselectionColor = inPreselectionColor;

            m_Color.a = alpha;
            m_SelectedColor.a = alpha;
            m_PreselectionColor.a = alpha;
        }

        public float Do(int id, Vector3 position, Vector3 discNormal, float radius)
        {
            Event e = Event.current;

            Quaternion discRot = Quaternion.LookRotation(discNormal);

            Plane discPlane = new Plane(discNormal, position);

            switch(e.GetTypeForControl(id))
            {
                case EventType.Layout:
                    {
                        Handles.CircleHandleCap(id, position, discRot, radius, EventType.Layout);
                        Handles.DrawSolidDisc(position, discNormal, radius);
                    }
                    break;

                case EventType.MouseDown:
                    {
                        if (HandleUtility.nearestControl == id && e.button == 0 && GUIUtility.hotControl == 0)
                        {
                            GUIUtility.hotControl = id;
                            m_CurrentMousePos = e.mousePosition;
                            Ray guiToWorld = HandleUtility.GUIPointToWorldRay(m_CurrentMousePos);
                            discPlane.Raycast(guiToWorld, out float enter);
                            m_StartingWorldMousePos = guiToWorld.GetPoint(enter);
                            m_DiffDir = (m_StartingWorldMousePos - position).normalized;
                            EditorGUIUtility.SetWantsMouseJumping(1);
                            m_StartingRadius = radius;
                            e.Use();
                        }
                    }
                    break;

                case EventType.MouseDrag:
                    {
                        if (GUIUtility.hotControl == id)
                        {
                            m_CurrentMousePos += e.delta;

                            Ray guiToWorld = HandleUtility.GUIPointToWorldRay(m_CurrentMousePos);
                            discPlane.Raycast(guiToWorld, out float enter);

                            Vector3 pt = guiToWorld.GetPoint(enter);

                            radius = Mathf.Abs(m_StartingRadius + Vector3.Dot(guiToWorld.GetPoint(enter) - m_StartingWorldMousePos, m_DiffDir));

                            GUI.changed = true;
                            e.Use();
                        }
                    }
                    break;

                case EventType.MouseUp:
                    {
                        if (GUIUtility.hotControl == id && e.button == 0)
                        {
                            GUIUtility.hotControl = 0;
                            EditorGUIUtility.SetWantsMouseJumping(0);
                            e.Use();
                        }
                    }
                    break;

                case EventType.MouseMove:
                    {
                        if (id == HandleUtility.nearestControl)
                            HandleUtility.Repaint();
                    }
                    break;

                case EventType.Repaint:
                    {
                        Color oldHandlesColor = Handles.color;

                        Handles.color = m_Color;


                        if (id == GUIUtility.hotControl)
                            Handles.color = m_SelectedColor;
                        else if (id == HandleUtility.nearestControl && GUIUtility.hotControl == 0)
                            Handles.color = m_PreselectionColor;

                        Handles.DrawSolidDisc(position, discNormal, radius);
                        Handles.CircleHandleCap(id, position, discRot, radius, EventType.Repaint);

                        if (id == GUIUtility.hotControl || id == HandleUtility.nearestControl && GUIUtility.hotControl == 0)
                            Handles.color = oldHandlesColor;
                    }
                    break;
            }

            return radius;
        }
    }
}

