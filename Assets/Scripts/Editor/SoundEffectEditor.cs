﻿#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

[CustomEditor(typeof(SoundEffect))]
public class SoundEffectEditor : Editor
{
    private ReorderableList m_Clips;
    private SerializedProperty m_MinPitch;
    private SerializedProperty m_MaxPitch;
    private SerializedProperty m_MinVolume;
    private SerializedProperty m_MaxVolume;

    private SerializedProperty m_RandomizeClip;
    private SerializedProperty m_RandomizeVolume;
    private SerializedProperty m_RandomizePitch;

    private AudioSource m_PreviewSource;

    void OnEnable()
    {
        m_Clips = ReorderableListUtility.CreateAutoLayout(serializedObject.FindProperty("m_Clips"));
        m_MinPitch = serializedObject.FindProperty("m_MinPitch");
        m_MaxPitch = serializedObject.FindProperty("m_MaxPitch");
        m_MinVolume = serializedObject.FindProperty("m_MinVolume");
        m_MaxVolume = serializedObject.FindProperty("m_MaxVolume");

        m_RandomizeClip = serializedObject.FindProperty("m_RandomizeClip");
        m_RandomizeVolume = serializedObject.FindProperty("m_RandomizeVolume");
        m_RandomizePitch = serializedObject.FindProperty("m_RandomizePitch");
        
        if (!m_RandomizeClip.boolValue && m_Clips.serializedProperty.arraySize == 0)
        {
            m_Clips.serializedProperty.ClearArray();
            m_Clips.serializedProperty.InsertArrayElementAtIndex(0);
            serializedObject.ApplyModifiedProperties();
        }

        m_PreviewSource = EditorUtility.CreateGameObjectWithHideFlags("Sound Effect Previewer", HideFlags.HideAndDontSave, typeof(AudioSource)).GetComponent<AudioSource>();
    }

    public override void OnInspectorGUI()
    {
        Color _guiColor = GUI.backgroundColor;

        EditorGUI.BeginChangeCheck();
        
        EditorGUILayout.PropertyField(m_RandomizeClip);

        if (m_RandomizeClip.boolValue)
            m_Clips.DoLayoutList();
        else
        {
            EditorGUILayout.BeginVertical(GUI.skin.box);
            EditorGUILayout.PropertyField(m_Clips.serializedProperty.GetArrayElementAtIndex(0), new GUIContent("Clip"));
            EditorGUILayout.EndVertical();
        }



        EditorGUILayout.Space();

        EditorGUILayout.PropertyField(m_RandomizePitch);

        if (m_RandomizePitch.boolValue)
        {
            GUI.backgroundColor = Color.yellow;
            EditorGUILayout.BeginVertical(GUI.skin.box);
            GUI.backgroundColor = _guiColor;
            EditorGUILayout.PropertyField(m_MinPitch);
            EditorGUILayout.PropertyField(m_MaxPitch);
            EditorGUILayout.EndVertical();
        }

        EditorGUILayout.Space();

        EditorGUILayout.PropertyField(m_RandomizeVolume);

        if (m_RandomizeVolume.boolValue)
        {
            GUI.backgroundColor = Color.cyan;
            EditorGUILayout.BeginVertical(GUI.skin.box);
            GUI.backgroundColor = _guiColor;
            EditorGUILayout.PropertyField(m_MinVolume);
            EditorGUILayout.PropertyField(m_MaxVolume);
            EditorGUILayout.EndVertical();
        }

        EditorGUILayout.Space();

        if (EditorGUI.EndChangeCheck())
            serializedObject.ApplyModifiedProperties();

        if (GUILayout.Button("Play")) ((SoundEffect)target).Play(m_PreviewSource);
    }
}

#endif
