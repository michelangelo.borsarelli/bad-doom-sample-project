﻿using UnityEngine;
using UnityEditor;
using CorgiLabs.EditorUtils;

[CustomEditor(typeof(Entity))]
public class EntityEditor : Editor
{
    SerializedProperty m_ChaseRadius;
    RadiusDiscHandle m_RadiusHandle;

    private int m_ID;

    private Transform m_TargetTransform;

    private void OnEnable()
    {
        m_ChaseRadius = serializedObject.FindProperty("m_ChaseRadius");
        m_RadiusHandle = new RadiusDiscHandle(Color.green, Color.blue, Color.magenta);
        m_TargetTransform = (target as Entity).transform;
        m_ID = GUIUtility.GetControlID(GetHashCode(), FocusType.Passive);
    }

    private void OnSceneGUI()
    {
        EditorGUI.BeginChangeCheck();
        m_ChaseRadius.floatValue = m_RadiusHandle.Do(m_ID, m_TargetTransform.position, m_TargetTransform.up, m_ChaseRadius.floatValue);
        if (EditorGUI.EndChangeCheck())
            serializedObject.ApplyModifiedProperties();
    }
}
