﻿using UnityEngine;

[CreateAssetMenu(menuName = "Item")]
public class Item : ScriptableObject
{
    public string DisplayName;
    public Sprite Icon;

    public ItemEffect[] Effects;

    public void Use(PlayerController user)
    {
        foreach (ItemEffect e in Effects)
            e.Apply(user);
    }
}


public abstract class ItemEffect : ScriptableObject
{
    public string DisplayDescription => DescriptionOverride != null && DescriptionOverride != string.Empty ? DescriptionOverride : Description;

    [TextArea]
    public string DescriptionOverride;

    protected abstract string Description { get; }

    public abstract void Apply(PlayerController user);
}