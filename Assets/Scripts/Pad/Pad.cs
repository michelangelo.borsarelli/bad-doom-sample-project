﻿using UnityEngine;

public class Pad : MonoBehaviour
{
    public PadType Type;

    public SpriteRenderer Icon;
    public MeshRenderer BaseRenderer;

    private PadEffect m_Effect;

    private void OnTriggerEnter(Collider other)
    {
        PlayerController pc = other.GetComponentInParent<PlayerController>();

        if (pc)
            m_Effect.Apply(pc);
    }

    private void Awake()
    {
        m_Effect = GetComponent<PadEffect>();

        Icon.sprite = Type.Icon;
        BaseRenderer.material.color = Type.BaseColor;
        GetComponentInChildren<Light>().color = Type.BaseColor;
    }

}
