﻿using UnityEngine;
using TypeReferences;

[CreateAssetMenu(menuName = "Pad Type")]
public class PadType : ScriptableObject
{
    [SerializeField, ClassExtends(typeof(PadEffect))]
    private ClassTypeReference m_EffectType;
    public System.Type EffectType => m_EffectType.Type;

    public Sprite Icon;
    public Color BaseColor;
}
