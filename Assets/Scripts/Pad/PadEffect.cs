﻿using UnityEngine;

public abstract class PadEffect : MonoBehaviour
{
    public abstract void Apply(PlayerController user);
}
