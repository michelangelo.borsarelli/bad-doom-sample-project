﻿public class EquipWeaponPadEffect : PadEffect
{
    public Weapon ContainedWeapon;

    public override void Apply(PlayerController user)
    {
        user.EquipWeapon(ContainedWeapon);
    }
}
