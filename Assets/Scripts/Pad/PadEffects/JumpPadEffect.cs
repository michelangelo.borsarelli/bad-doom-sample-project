﻿using UnityEngine;

public class JumpPadEffect : PadEffect
{
    public float AppliedForce;

    public override void Apply(PlayerController user)
    {
        user.rigidbody.AddForce(AppliedForce * Vector3.up, ForceMode.Impulse);
    }
}
