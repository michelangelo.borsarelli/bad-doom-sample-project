﻿public class ItemPadEffect : PadEffect
{
    public Item ContainedItem;

    public override void Apply(PlayerController user)
    {
        user.CurrentItem = ContainedItem;
    }
}
