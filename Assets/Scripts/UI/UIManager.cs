﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class UIManager : MonoBehaviour
{
    [SerializeField] private PlayerController m_Player;
    [Space]
    [SerializeField] private Slider m_Healthbar;
    [SerializeField] private GameObject m_ItemArea;
    [SerializeField] private GameObject m_WeaponArea;
    [Space]
    [SerializeField] private Image m_PowerUpTint;
    [SerializeField] private Image m_PowerUpVignette;
    [Space]
    [SerializeField] private Image m_DamagedVignette;
    [Space]
    [SerializeField] private float m_HealthTweenTime;
    [Space]
    [SerializeField] private float m_PowerUpFadeTime;
    [SerializeField, Range(0,1)] private float m_PowerUpTintAlpha;
    [SerializeField, Range(0,1)] private float m_PowerUpVignetteAlpha;
    [Space]
    [SerializeField] private float m_DamagedFadeTime;

    private RectTransform m_HealthbarTransform;
    private TextMeshProUGUI m_HealthValue;
    private Image m_ItemIcon;
    private Image m_WeaponIcon;

    private float m_BarSizePerHealthPoint;

    private void Awake()
    {
        m_HealthbarTransform = m_Healthbar.GetComponent<RectTransform>();
        m_HealthValue = m_Healthbar.GetComponentInChildren<TextMeshProUGUI>();
        m_ItemIcon = m_ItemArea.GetComponentInChildren<Image>();
        m_WeaponIcon = m_WeaponArea.GetComponentInChildren<Image>();

        m_Healthbar.maxValue = m_Player.MaxHealth;
        m_Healthbar.value = m_Player.MaxHealth;
        m_HealthValue.text = m_Player.MaxHealth.ToString();

        m_BarSizePerHealthPoint = m_HealthbarTransform.sizeDelta.x / m_Player.MaxHealth;
    }

    private void OnEnable()
    {
        m_Player.OnItemChanged += OnItemChanged;
        m_Player.OnHealthChanged += OnHealthChanged;
        m_Player.OnDamaged += OnDamaged;
        m_Player.OnMaxHealthChanged += OnMaxHealthChanged;
        m_Player.OnPoweredUpChanged += OnPoweredUpChanged;
        m_Player.OnWeaponChanged += OnWeaponChanged;
    }

    private void OnDisable()
    {
        m_Player.OnItemChanged -= OnItemChanged;
        m_Player.OnHealthChanged -= OnHealthChanged;
        m_Player.OnDamaged -= OnDamaged;
        m_Player.OnMaxHealthChanged -= OnMaxHealthChanged;
        m_Player.OnPoweredUpChanged -= OnPoweredUpChanged;
        m_Player.OnWeaponChanged -= OnWeaponChanged;
    }

    private void Start()
    {
        if (m_Player.CurrentItem == null)
            m_ItemArea.SetActive(false);
        else
            m_ItemIcon.sprite = m_Player.CurrentItem.Icon;
    }

    public void OnDamaged()
    {
        m_DamagedVignette.DOFade(1, m_DamagedFadeTime).SetLoops(2, LoopType.Yoyo);
    }

    public void OnHealthChanged()
    {
        m_Healthbar.DOValue(m_Player.Health, m_HealthTweenTime);
        m_HealthValue.text = Mathf.RoundToInt(m_Player.Health).ToString();
    }

    public void OnMaxHealthChanged()
    {
        m_Healthbar.maxValue = m_Player.MaxHealth;
        m_HealthbarTransform.DOSizeDelta(new Vector2(m_Player.MaxHealth * m_BarSizePerHealthPoint, m_HealthbarTransform.sizeDelta.y), m_HealthTweenTime);
    }

    public void OnItemChanged()
    {
        if (m_Player.CurrentItem)
        {
            m_ItemIcon.sprite = m_Player.CurrentItem.Icon;
            m_ItemArea.SetActive(true);
        }
        else
            m_ItemArea.SetActive(false);
    }

    public void OnWeaponChanged()
    {
        if (m_Player.CurrentWeapon)
        {
            m_WeaponIcon.sprite = m_Player.CurrentWeapon.Class.Icon;
            m_WeaponArea.SetActive(true);
        }
        else
            m_WeaponArea.SetActive(false);
    }

    public void OnPoweredUpChanged()
    {
        if (m_Player.IsPoweredUp)
        {
            m_PowerUpVignette.DOFade(m_PowerUpVignetteAlpha, m_PowerUpFadeTime);
            m_PowerUpTint.DOFade(m_PowerUpTintAlpha, m_PowerUpFadeTime);
        }
        else
        {
            m_PowerUpVignette.DOFade(0, m_PowerUpFadeTime);
            m_PowerUpTint.DOFade(0, m_PowerUpFadeTime);
        }
    }
}
