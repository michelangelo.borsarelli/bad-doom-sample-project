﻿using UnityEngine;

public class AnimatorHandler : MonoBehaviour
{
    public Animator Animator { get; private set; }

    private Entity m_Entity;

    private void Awake()
    {
        Animator = GetComponent<Animator>();
        m_Entity = GetComponentInParent<Entity>();
    }

    private void Update()
    {
        Animator.SetFloat("Speed", m_Entity.Agent.velocity.sqrMagnitude);
    }
}
