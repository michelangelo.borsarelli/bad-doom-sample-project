﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AIStateType
{
    Idle,
    Chase,
    Attack
}

public abstract class AIState : ScriptableObject
{
    public AIStateType StateType;

    private Entity m_Entity;
    private StateMachine m_Fsm;

    public bool Enter()
    {
        return m_Entity && m_Fsm && OnEnter();
    }

    protected virtual bool OnEnter()
    {
        return true;
    }

    public abstract void Update();

    public bool Exit()
    {
        return m_Entity && m_Fsm && OnExit();
    }

    protected virtual bool OnExit()
    {
        return true;
    }
}
