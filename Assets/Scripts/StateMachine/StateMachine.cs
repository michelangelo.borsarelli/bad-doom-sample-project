﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachine : MonoBehaviour
{
    [SerializeField]
    AIStateType m_StartingState;

    [SerializeField]
    private List<AIState> m_StateTemplates;

    public AIState CurrentState { get; private set; }
    public AIState PreviousState { get; private set; }

    private Dictionary<AIStateType, AIState> m_States;

    private void Awake()
    {
        m_States = new Dictionary<AIStateType, AIState>();

        foreach(AIState _template in m_StateTemplates)
        {
            if (!m_States.ContainsKey(_template.StateType))
                m_States.Add(_template.StateType, Instantiate(_template));
        }
    }

    private void OnValidate()
    {
        if (!m_StateTemplates.Find(s => s.StateType == m_StartingState))
        {
            if (m_StateTemplates.Count > 0)
                m_StartingState = m_StateTemplates[0].StateType;
            else
                m_StartingState = default;
        }
    }

    public void EnterState(AIStateType stateType)
    {
        if (m_States.ContainsKey(stateType))
            EnterState(m_States[stateType]);
    }

    public void EnterState(AIState state)
    {
        if (state.Enter() && CurrentState.Exit())
        {
            PreviousState = CurrentState;
            CurrentState = state;
        }
    }
}
