﻿using UnityEngine;
using DG.Tweening;
using System.Collections.Generic;

[CreateAssetMenu(menuName = "Model Manipulations/Flash")]
public class ModelFlash : ScriptableObject
{
    [SerializeField] float m_FlashDuration;
    [SerializeField] Color m_FlashColor;

    private HashSet<Material> m_TweeningMaterials = new HashSet<Material>();

    public void DoFlash(Renderer renderer)
    {
        foreach(Material _mat in renderer.materials)
        {
            if (!m_TweeningMaterials.Contains(_mat))
            {
                Material _curr = _mat;
                m_TweeningMaterials.Add(_mat);
                _mat.DOColor(m_FlashColor, m_FlashDuration / 2).SetLoops(2, LoopType.Yoyo).OnComplete(() => m_TweeningMaterials.Remove(_curr));
            }
        }
    }
}
