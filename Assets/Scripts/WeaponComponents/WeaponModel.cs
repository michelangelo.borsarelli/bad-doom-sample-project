﻿using UnityEngine;

public class WeaponModel : MonoBehaviour
{
    public Transform[] SpawnPoints;
    public Animator Animator;

    private void Awake()
    {
        Animator = GetComponent<Animator>();
    }
}
