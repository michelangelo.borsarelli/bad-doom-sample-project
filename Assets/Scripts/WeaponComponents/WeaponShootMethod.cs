﻿using UnityEngine;

public abstract class WeaponShootMethod : ScriptableObject
{
    public abstract void Shoot(float damage, WeaponModel model, PlayerController weaponOwner);
}
