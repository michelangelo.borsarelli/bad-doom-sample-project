﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class Movement : ScriptableObject
{
    public virtual void Init(Transform tr) { }
    public abstract void MoveUpdate(Transform tr);
}
