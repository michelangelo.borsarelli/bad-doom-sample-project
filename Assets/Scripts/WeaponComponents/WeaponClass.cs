﻿using UnityEngine;

[CreateAssetMenu(menuName ="Weapon Class")]
public class WeaponClass : ScriptableObject
{
    public string DisplayName;
    public Sprite Icon;
    public SoundEffect PickupSfx;
}
