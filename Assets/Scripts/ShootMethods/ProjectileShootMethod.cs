﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Shoot Methods/Projectiles")]
public class ProjectileShootMethod : WeaponShootMethod
{
    private const float MISS_TARGET_DISTANCE = 50.0f;

    public ProjectileTemplate ProjectileTemplate;

    public override void Shoot(float damage, WeaponModel model, PlayerController weaponOwner)
    {
        Projectile _curr;

        Vector3 _targetPos;

        if (Physics.Raycast(weaponOwner.FpsCamera.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 0)), weaponOwner.FpsCamera.transform.forward, out RaycastHit hitInfo))
            _targetPos = hitInfo.point;
        else
            _targetPos = weaponOwner.transform.position + weaponOwner.FpsCamera.transform.forward * MISS_TARGET_DISTANCE;

        foreach (Transform _spawn in model.SpawnPoints)
        {
            _curr = Projectile.CreateInstance(ProjectileTemplate, _spawn.position, Quaternion.LookRotation((_targetPos - _spawn.position).normalized));
            _curr.transform.Rotate(Vector3.forward, _spawn.rotation.eulerAngles.z, Space.Self);
            _curr.OnHit = (projectile, hit) =>
            {
                hit.GetComponent<IDamageable>()?.TakeDamage(damage);
                Destroy(projectile);
            };
        }
    }
}
