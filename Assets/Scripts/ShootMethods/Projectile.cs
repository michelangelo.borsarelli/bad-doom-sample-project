﻿using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public new Transform transform { get; private set; }

    public System.Action<GameObject, GameObject> OnHit;

    private GameObject m_Model;

    private Movement m_Movement;


    public static Projectile CreateInstance(ProjectileTemplate template, Vector3 pos, Quaternion rot)
    {
        Projectile _proj = new GameObject("Projectile", typeof(Projectile), typeof(Rigidbody)).GetComponent<Projectile>();

        _proj.GetComponent<Rigidbody>().isKinematic = true;

        _proj.transform = _proj.GetComponent<Transform>();
        _proj.transform.SetPositionAndRotation(pos, rot);
        _proj.LoadTemplate(template);

        Destroy(_proj.gameObject, 6);

        return _proj;
    }

    public void LoadTemplate(ProjectileTemplate template)
    {
        m_Model = Instantiate(template.Model, transform.position, transform.rotation, transform);
        m_Movement = Instantiate(template.Movement);
        m_Movement.Init(transform);
    }

    private void Update()
    {
        m_Movement.MoveUpdate(transform);
    }

    private void OnTriggerEnter(Collider other)
    {
        OnHit?.Invoke(this.gameObject, other.gameObject);
    }
}
