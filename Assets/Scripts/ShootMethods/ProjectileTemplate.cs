﻿using UnityEngine;
using TypeReferences;

[CreateAssetMenu(menuName = "Projectile")]
public class ProjectileTemplate : ScriptableObject
{
    public GameObject Model;

    [ClassExtends(typeof(Movement))]
    public ClassTypeReference MovementType;

    public Movement Movement;
}
