﻿using UnityEngine;

[CreateAssetMenu(menuName ="Shoot Methods/Hitscan")]
public class HitscanShootMethod : WeaponShootMethod
{
    private static readonly Vector3 MID_VIEWPORT = new Vector3(0.5f, 0.5f, 0);

    public override void Shoot(float damage, WeaponModel model, PlayerController weaponOwner)
    {
        Vector3 origin = weaponOwner.FpsCamera.ViewportToWorldPoint(MID_VIEWPORT);

        if (Physics.Raycast(origin, weaponOwner.FpsCamera.transform.forward, out RaycastHit hitInfo))
            hitInfo.collider.GetComponentInParent<IDamageable>()?.TakeDamage(damage);
        else
            Debug.Log("Miss");
    }
}
