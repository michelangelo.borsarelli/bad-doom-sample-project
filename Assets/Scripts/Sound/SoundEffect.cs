﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CorgiLabs.Utility;

[CreateAssetMenu(menuName = "Sounds/Sound Effect")]
public class SoundEffect : ScriptableObject
{
    private static ObjectPool<AudioSource> m_SourcesPool =
        new ObjectPool<AudioSource>
        (
            delegate (AudioSource source)
            {
                return !source.isPlaying;
            },
            delegate ()
            {
                return new GameObject("AudioSource", typeof(AudioSource)).GetComponent<AudioSource>();
            }
        );

    [SerializeField] private bool m_RandomizeClip;

    [SerializeField] private AudioClip[] m_Clips;

    [SerializeField] private bool m_RandomizePitch;

    [Space]
    [SerializeField] float m_MinPitch;
    [SerializeField] float m_MaxPitch;

    [SerializeField] private bool m_RandomizeVolume;

    [Space]
    [SerializeField] float m_MinVolume;
    [SerializeField] float m_MaxVolume;

    public void Play(AudioSource source)
    {
        source.volume = m_RandomizeVolume ? Random.Range(m_MinVolume, m_MaxVolume) : 1;
        source.pitch = m_RandomizePitch ? Random.Range(m_MinPitch, m_MaxPitch) : 1;

        source.clip = m_RandomizeClip ? m_Clips[Random.Range(0, m_Clips.Length)] : m_Clips[0];

        source.Play();
    }

    public void Play(Vector3 position)
    {
        AudioSource source = m_SourcesPool.Get();
        source.transform.position = position;
        Play(source);
    }
}
